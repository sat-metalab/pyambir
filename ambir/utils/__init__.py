__author__ = "Thomas Piquet" 
__copyright__ = "Copyright 2021, Société des arts technologiques" 
__credits__ = ["Thomas Piquet", "Michał Seta", "Patrick Dupuis", "Benjamin Langlois"] 
__license__ = "GPLv3" 
__maintainer__ = "Thomas Piquet" 
__email__ = "metalab@sat.qc.ca" 
__status__ = "Development" 
__version__ = "0.0.1"
