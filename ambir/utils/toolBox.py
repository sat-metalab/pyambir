import os
from typing import List
from math import sqrt
from tokenize import String
import numpy as np

import soundfile as sf

def get_ambisonic_recording(path: str) -> List[str]:
    """
    Get all ambisonic recording from path.

    :param filename: path to the directory with ambisonic files
    :return: list of path of individual ambisonic file
    """
    file_list = []
    for f in os.listdir(path):
        if ".wav" in f:
            file_path = os.path.join(path,f)
            try:
                get_ambisonic_order(file_path)
                file_list.append(file_path)
            except Exception as e:
                print(e)

    return sorted(file_list)


def get_ambisonic_order(filename: str) -> int:
    """
    Returns the ambisonic order of the file. Handles up to 4th order.
    Raises an exception if the file is not compatible.

    :param filename: path to the file to analyse
    :return: ambisonic order of the file
    :raises Exception: if file format is not supported
    """
    wave, fs = sf.read(filename)
    nb_channels = len(wave[0])

    if nb_channels == 1:
        order = 0
    elif nb_channels == 4:
        order = 1
    elif nb_channels == 9:
        order = 2
    elif nb_channels == 16:
        order = 3
    elif nb_channels == 25:
        order = 4
    else:
        raise Exception(f"Unsupported file format {filename}")

    return order


CONVERSION_FACTOR = np.array([1, # order 0
                     sqrt(3), sqrt(3), sqrt(3), # order 1
                     sqrt(5), sqrt(5), sqrt(5), sqrt(5), sqrt(5), # order 2
                     sqrt(7), sqrt(7), sqrt(7), sqrt(7), sqrt(7), sqrt(7), sqrt(7), # order 3
                     1/3, 1/3, 1/3, 1/3, 1/3, 1/3, 1/3, 1/3, 1/3]) # order 4


def convert_SN3D_to_N3D(filename: str) -> String:
    """
    Convert Ambix file normalization from SN3D to N3D. For Ambisonic order
    0, 1, 2, 3, 4, and ACN channel ordering. Raises an exception if
    the file is not compatible.

    :param filename: path to the file to convert
    :return: path to the converted file
    :raises Exception: if file format is not supported
    """
    wave, fs = sf.read(filename)
    max_wave = np.amax(wave)

    order = get_ambisonic_order(filename)

    if order == 0:
        # no conversion needed
        return filename
    elif order == 1:
        conv_factor = CONVERSION_FACTOR[:4]
    elif order == 2:
        conv_factor = CONVERSION_FACTOR[:9]
    elif order == 3:
        conv_factor = CONVERSION_FACTOR[:16]
    elif order == 4:
        conv_factor = CONVERSION_FACTOR

    new_wave = wave * conv_factor
    new_filename = os.path.splitext(filename)[0] + "_to_N3D" + os.path.splitext(filename)[1]

    # normalize if needed
    max_new_wave = np.amax(new_wave)
    if max_new_wave > 1:
        new_wave = new_wave * (max_wave/max_new_wave)

    sf.write(new_filename, new_wave, fs)

    return new_filename


def convert_to_Bformat(input_filename: str, filter_fileName: str) -> None:
    """
    Convert A format recording to B format. This use conversion matrix
    the same way found here:
    http://www.angelofarina.it/X-volver.htm
    Multiple filter source can be find here
    http://pcfarina.eng.unipr.it/Public/Xvolver/Filter-Matrices/Aformat-2-Bformat/Zylia-Jul-2020/

    :param input_filename: filename of the recording to convert
    :param filter_filename: filename of the filter to use for the
                            conversion. Usually a wav file
    """
    input, f = sf.read(input_filename, always_2d=True)
    # trim input up to 19s
    input = input[:19*f,]
    nbInputChannels = np.shape(input)[1]
    outputFileName = input_filename.replace("wav", "_B.wav")

    filter, f = sf.read(filter_fileName, always_2d=True)
    nbFilterChannels = np.shape(filter)[1]

    bformat = np.zeros((np.shape(input)[0], np.shape(filter)[1]))
    for i in range(nbFilterChannels):
        currentSlice = 0
        for j in range(nbInputChannels):
            currentSlice = j * 4096
            filterSlice = filter[currentSlice:currentSlice + 4096, i]
            bformat[:, i] = bformat[:, i] + ssig.fftconvolve(input[:, j],
                                                             filterSlice,
                                                             mode='same')

    sf.write(outputFileName, bformat, f, 'PCM_24')
