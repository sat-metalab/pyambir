from typing import List
from scipy import signal

import soundfile as sf
from pyroomacoustics.experimental import rt60


def get_reverberation_time_per_octave(filename: str,
                                      decay_db: int = 60) -> List[float]:
    """
    Compute RT60 time per octave from 80Hz to 20kHz (8 bands total)

    :param filename: Filename of IR to analyse
    :param decay_db: quantity of decay (default at 60, for RT60
                        computation)
    :return: List of all decay time per frequency band in seconds (Low
                frequency to high frequency)
    """
    maxFreq = 20000.0
    centerFreq = []

    highcut = maxFreq
    lowcut = highcut / 2.

    IR, fs = sf.read(filename)
    inputIR = IR[:, 0]

    reverberationValues = []
    while lowcut > 40:
        currentTaps = _bandpass_firwin(lowcut, highcut, fs)

        filtered_IR = signal.lfilter(currentTaps, 1.0, inputIR)

        RT = rt60.measure_rt60(filtered_IR, fs=fs, decay_db=decay_db)
        reverberationValues.append(RT)

        # new band
        centerFreq.append((highcut+lowcut)/2)
        highcut = lowcut
        lowcut = highcut / 2


    # reverse order: low frequency to high frequency
    return reverberationValues[::-1], centerFreq[::-1]


def get_reverberation_time(filename: str,
                           decay_db: int = 60) -> float:
    """
    Compute Reverberation time

    :param filename: Filename of IR to analyse
    :param decay_db: quantity of decay (default at 60, for RT60
                        computation)
    """
    IR, fs = sf.read(filename)
    inputIR = IR[:, 0]

    return rt60.measure_rt60(inputIR, fs=fs, decay_db=decay_db)


def _bandpass_firwin(lowcut,
                     highcut,
                     fs) -> List[float]:
    """
    Compute coeficients for band pass

    :param lowcut: lowcut frequency in Hz
    :param highcut: highcut frequency in Hz
    :param fs: sample rate in Hz
    :return: List of filter coeficient
    """
    taps = signal.firwin(4000, [lowcut, highcut],
                         fs=fs, window='hamming', pass_zero=False)
    return taps

def _bandpass_butter(lowcut,
                     highcut,
                     fs) -> List[float]:
    pass


# https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.hilbert.html