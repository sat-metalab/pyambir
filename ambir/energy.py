from typing import Any, List
from ambir.utils import spherical_harmonics as sh
import soundfile as sf
import numpy as np


class Energy:
    def __init__(self,
                 IR: List[Any],
                 fs: int,
                 analysis_length: int,
                 phi: int = 90,
                 theta: int = 180) -> None:
        """
        Compute spherical energy of an ambisonic IR.

        :param IR_filepath: Filename of IR to analyse
        :param analysis_length: window of analysis
        :param phi: colatitude meshing (number of subdivisions, default 90)
        :param theta: azimuthal meshing (number of subdivisions, default 180)
        """
        self.sample_step = analysis_length
        self.IR = IR
        self.fs = fs

        if len(IR[0]) == 4:
            order = 1
        if len(IR[0]) == 9:
            order = 2
        elif len(IR[0]) == 16:
            order = 3
        elif len(IR[0]) == 25:
            order = 4

        # Spherical variables
        self.phi_num = phi
        self.theta_num = theta
        self.phi, self.theta = np.meshgrid(
            np.linspace(-np.pi / 2, np.pi / 2, self.phi_num),
            np.linspace(0, 2 * np.pi, self.theta_num))

        self.spherical_harmonics = np.zeros(
            (order + 1, 2 * order + 1, self.theta_num, self.phi_num))
        for degree in range(0, order + 1):
            for index in range(-degree, degree + 1):
                self.spherical_harmonics[degree, index] = sh.real_spherical_harmonic(degree, index, self.theta, self.phi)
        self.energy = np.zeros((self.theta_num,
                                self.phi_num))
        self.pressure = np.zeros((self.theta_num,
                                  self.phi_num,
                                  self.sample_step))

    def generate_spherical_energy_matrix(self,
                                         sample_position: int):
        # compute energy
        for i in range(0, self.sample_step - 1):
            self.pressure[:, :, i] = sh.build_angular_field_acn(
                self.IR[i + sample_position, :], self.spherical_harmonics)
        # Compute self.energy over specified sample steps
        self.energy[:, :] = np.sum(self.pressure[:, :, :]**2, 2)
        return self.energy
