# pyAmbIR

pyAmbir is a Python library containing multiple tools to create and analyse ambisonic impulse response.

## Installing AmbIR
In order to install pyAmbIR, clone it and from within its directory execute `python -m pip install`. (or `python -m pip install -e .` if you also want to hack at it).

You will need to use at least Python 3.6.

## Example scripts
5 examples scripts using pyAmbir are available:
- `examples/IR_recording.py`
- `examples/visualize_ambisonic_IR.py`
- `examples/formatConverter.py`
- `examples/reverberation_time.ipynb`
- `examples/RIR_interpolation/interpolate_irs.py`

We also provide a sample impulse response: `examples/centech_center_3rd_order.wav`

This is a 3rd order ambisonic impulse response recorded in the former Montreal planetarium at the center of the dome, where flutter echo is at its maximum.

### IR recording script
This script was used to record ambisonic IR in the former montreal planetarium. This is mostly a use case of `exponentialSineSweep.py` module.

Playback and recording is done using `python-zita-jacktools 1.5.3`, all wrapped up in `jacksetup.py`.

### Visualization script
An example interactive script is provided to visualize ambisonic impulse response: `visualize_ambisonic_IR.py`. This script is mostly a use case of the `eneregy.py` module.
```
$ ./visualize_ambisonic_IR.py -h
usage: visualize_ambisonic_IR.py [-h] -i INPUTRIR [-p INPUTPICTURE] [-a ALPHA]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTRIR, --inputRIR INPUTRIR
                        Path to the ambisonic RIR
  -p INPUTPICTURE, --inputPicture INPUTPICTURE
                        Path to a 360 equirectangular picture to be incorporated in the energy window
  -a ALPHA, --alpha ALPHA
                        value of alpha blending if an inputPicture is provided, default to 0.7 if not provided
```

![visualize_ambisonic_IR.py](./ambir_visualize.png)

To launch it:
```
$ python3 examples/visualize_ambisonic_IR.py -i examples/centech_center_3rd_order.wav
```

It will display 3 windows:
- **Spectrogram**: a spectrogram of the IR, with the waveform overlayed. The displayed values are from chanel 0 of the ambisonic IR (omni directional IR).
- **Energy**: represent spherical energy repartition of the ambisonic IR.
- **Energy Sliders**: Sliders to fine tune representation of the **Energy** window, plus an animate button.

At start-up the energy window display the energy repartitation at the loudest point in the IR.

### Spectrogram window
Clicking on the waveform will display the energy repartition at that point in time.

Use the zoom function to select the part of the IR you want to render with animate button from the **Energy Sliders** window.

### Energy window
Use left or right arrow key to move the analysis point in time. The time shift is set in sample in the **Energy Sliders** window.

The image bellow shows the overlaying of an equirectangular picture using option `-p INPUTPICTURE`
![](ambir_energy_overlay.png)

### Energy Sliders windows
- **Max sensitivity**: Sets the max sensitivity of the color normalization. By default is set to the max ernergy point in the IR.
- **Sensitivity range**: Sets the sensitivity range for the color normamization
- **Frame size**: Sets the frame size use to render spherical energy repartition (in samples).
- **arrow key inc**: Sets the number of samples to shift when using arrow key in the **Energy** window. This value is also used with the animate button to define the number of sample between two frames.
- **animate button**: Generates an mp4 video file of the spherical energy repartition. The start and end the render is defined by the part of the waveform visible in **Spectrogram** window. To set it use the zoom function from it. *Requires ffmpeg installed on your computer*

### Normalisation format converter script

This script is to convert normalization format of ambisonic file. It convert ambix format with `SN3D` normalisation to ambix with `N3D` normalisation. It is a batch converter, ie. it convert all ambisonic file inside a directory.

```
$ python3 formatConverter.py <PATH TO AMBIXs>
```

### Reverberation time computation Notebook

This notebook offers few ways of visuallizing the results of reverberation time analysis.

The notebook format is handy to explore data and generate visualization (graphics, tables).

[Reverberation time notebook](examples/reverberation_time.ipynb)

### Room impulse response interpolation

In `examples/RIR_interpolation/` you will find an exemple script to create interpolated impulse responses for recorded RIR. Details about usage of the script can be found [here](examples/RIR_interpolation/README_interpolate.md).

This script has been used to create 6 degrees of freedom navigation in these videos:
- Centech: https://vimeo.com/697114857
- Maison symphonique: https://vimeo.com/697114857