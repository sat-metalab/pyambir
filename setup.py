from setuptools import setup

setup(name='AmbIR',
      version='0.2',
      description='Ambisonic Impulse Response toolbox',
      url='https://gitlab.com/sat-metalab/tools/ambir',
      author='Thomas Piquet',
      author_email='tpiquet@sat.qc.ca',
      install_requires=['numpy', 'scipy', 'soundfile', 'matplotlib', 'pyroomacoustics'],
      packages=['ambir'],
      zip_safe=False)
