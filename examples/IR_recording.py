#!/usr/bin/python3

import numpy as np
import soundfile as sf
import scipy.signal as ssig
from signal import signal, SIGINT
from typing import List
from ambir import exponentialSineSweep

import jacksetup

RECORD_DURATION = 38

def generate_polar_measurement_plan(radius_list: List[int],
                     measurement_points: List[int]) -> List[str]:
    """
    Generate a list of file name for the IR measurement session.
    Filename contains the polar coordinate for the placement of the
    mic or speaker.
    :param radius_list: list of angle
    :param measurement_points: list of distance from the center
    :return: List of file name combining radius_list and
             measurement_points
    """
    out = []
    for i in radius_list:
        fileName = "REC_a" + str(i)
        for points in measurement_points:
            out.append(fileName + "_P" + str(points) + "m")
    return out

def handle_SIGNINT(signal_received, frame):
    """
    Callback to intercept SIGINT and stop current recording.
    SIGINT signal halts python subprocess, this is why we need to
    reconnect zylia microphone in jack.
    """
    print('stop current recording')
    jack_env.stop_recording()
    jack_env.reconnect_zylia()

def convert_to_Bformat(input_filename: str, filter_fileName: str) -> None:
    """
    Convert A format recording to B format. This use conversion matrix
    the same way found here:
    http://www.angelofarina.it/X-volver.htm
    Multiple filter source can be find here
    http://pcfarina.eng.unipr.it/Public/Xvolver/Filter-Matrices/Aformat-2-Bformat/Zylia-Jul-2020/

    :param input_filename: filename of the recording to convert
    :param filter_filename: filename of the filter to use for the
                            conversion. Usually a wav file
    """
    input, f = sf.read(input_filename, always_2d=True)
    nbInputChannels = np.shape(input)[1]
    outputFileName = input_filename.replace("wav","_B.wav")

    filter, f = sf.read(filter_fileName, always_2d=True)
    nbFilterChannels = np.shape(filter)[1]

    bformat = np.zeros((np.shape(input)[0], np.shape(filter)[1]))
    for i in range(nbFilterChannels):
        currentSlice = 0
        for j in range(nbInputChannels):
            currentSlice = j * 4096
            filterSlice = filter[currentSlice:currentSlice+4096,i]
            bformat[:, i] = bformat[:, i] + ssig.fftconvolve(input[:, j], filterSlice, mode='same')

    sf.write(outputFileName, bformat, f, 'PCM_24')

jack_env = None

if __name__ == "__main__":
    # handle ctrl+c
    signal(SIGINT, handle_SIGNINT)

    measureAngle = [0,1]
    measurePoints = [1]
    plan = generate_polar_measurement_plan(measureAngle, measurePoints)

    # Prepare buffer for playing
    ess = exponentialSineSweep.ExponentialSineSweep()
    ess.generate_ess()
    ess_for_jacktools = np.float32(ess.ess)

    jack_env = jacksetup.JackSetup(2, ess_for_jacktools,
                                   RECORD_DURATION)

    # Jack plumbing
    jack_env.connect_audiodice(1)
    # jack_env.connect_audiodice_single_speaker(1,12)

    for measureName in plan:
        redo = True
        while redo:
            print("-------------------------------")
            print("Will measure: " + measureName)
            input("Press Enter when ready to record")

            # play / record sweep
            jack_env.play_record(measureName)

            # check function docstring to obtain filter
            # A2B-Zylia-3E-Jul2020.wav
            convert_to_Bformat(measureName + ".wav",
                             "A2B-Zylia-3E-Jul2020.wav")

            # convert IR
            ess.deconv_ess(measureName + "_B.wav",
                           "IR_" + measureName + "_B.wav")

            print("-------------------------------")
            print("measurement finished")
            redoStr = input("redo ? y/N")

            if redoStr != "y":
                redo = False
