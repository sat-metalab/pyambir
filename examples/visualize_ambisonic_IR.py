import sys
import os
import glob
import subprocess
import argparse
from matplotlib import cm, colors
from matplotlib.backend_bases import MouseEvent
from matplotlib.widgets import Slider, Button
import matplotlib.pyplot as plt
import soundfile as sf
import numpy as np
from scipy import signal

from ambir import energy, reverberationTime


class Visualize:
    def __init__(self, filename: str,
                 overlayImage: str = None,
                 alpha: float = 1.0,
                 color_scheme = cm.plasma) -> None:
        self.current_arrow_key_increment = 50
        self.current_analysis_len = 100

        # read file
        self.filename = filename
        self.IR, self.fs = sf.read(filename)
        self.inputIR = self.IR[:, 0]

        self.overlayImage = overlayImage
        self.alpha = alpha
        self.color_scheme = color_scheme

        self.energy = energy.Energy(self.IR, self.fs,
                                    self.current_analysis_len)

    def display_wav(self) -> None:
        """
        Create 3 windows for IR analysis / visualisation:
        - IR spectrogram.
        - IR energy visualisation.
        - Windows containing sliders to customize energy visualisation.
        """
        # guesstimate the loudest part
        # put max peak in the middle of analysis window
        self.current_sample = self.inputIR.argmax() - int(self.energy.sample_step / 2)
        self.max_energy = self.energy.generate_spherical_energy_matrix(self.current_sample).max()

        self._create_spectrogram_window(self.filename)
        self._create_slider_window()
        self._create_energy_window()

        print(f"RT30: {reverberationTime.get_reverberation_time(self.filename, 30)}s")

        self._generate_energy_image()
        plt.show()


    def _create_IR_window(self) -> None:
        """
        Creates a window representing the waveform of the IR.
        """
        self.fig_IR, self.ax_IR = plt.subplots()
        plt.connect('button_press_event', self._on_click)
        plt.connect('key_press_event', self._on_key_press)

        self.ax_IR.set_xlabel("Time (s)")
        self.Time = np.linspace(0,
                                len(self.inputIR) / self.fs,
                                num=len(self.inputIR))
        self.ax_IR.plot(self.Time, self.inputIR)
        self.ax_IR.set_xlim([0, self.ax_IR.get_xlim()[1]])

    def _create_spectrogram_window(self,
                                   title: str,
                                   display_wave: bool = True) -> None:
        """
        Creates a window representing the spectrogram of the IR.
        :param title: title of the graph
        :param dispay_wav: disply waveform on top of the spectrogram
        """
        # TODO: add button for display wave
        #       slider for fft resolution

        axes_dimensions = [0.1, 0.1, 0.8, 0.8]

        N = 512  # Number of point in the fft
        f, t, Sxx = signal.spectrogram(self.inputIR,
                                       self.fs,
                                       window=signal.blackman(N),
                                       nfft=N,
                                       scaling='density')

        self.fig_IR = plt.figure(num='Spectrogram')
        plt.connect('button_press_event', self._on_click)

        self.ax_IR = self.fig_IR.add_axes(axes_dimensions)
        self.ax_IR.pcolormesh(t, f, 10 * np.log10(Sxx))  # dB spectrogram

        plt.ylabel('Frequency [Hz]')
        plt.xlabel('Time [seg]')
        plt.title('Spectrogram of ' + title, size=16)

        if display_wave:
            axe2 = self.fig_IR.add_axes(axes_dimensions)
            axe2.plot(self.inputIR, color='black')
            axe2.set_xlim(0, len(self.inputIR))
            axe2.patch.set_alpha(0.1)
            axe2.set_axis_off()

    def _create_energy_window(self) -> None:
        """
        Creates empty energy windows.
        """
        # prepare energy plot
        self.fig_energy, self.ax_energy = plt.subplots(frameon=False,
                                                       num='Energy')
        self.ax_energy = self.fig_energy.add_axes([0, 0, 1, 1])
        self.ax_energy.axis('off')
        plt.connect('key_press_event', self._on_key_press)

    def _create_slider_window(self) -> None:
        """
        Create a windows to set parameter for energy analysis.
        """
        # add sliders window
        self.fig_sliders, self.ax_sliders = plt.subplots(num='Energy Sliders')
        self.ax_sliders.axis('off')

        # max sensitivity
        self.sensitivity_slider_ax = self.fig_sliders.add_axes([0.25, 0.20,
                                                                0.65, 0.03])
        self.sensitivity_slider = Slider(self.sensitivity_slider_ax,
                                         'Max sensitivity',
                                         1,
                                         1000.0,
                                         valinit=self.max_energy)
        self.sensitivity_slider.on_changed(self._sliders_on_changed)

        # sensity range
        self.sensitivity_range_slider_ax = self.fig_sliders.add_axes([0.25,
                                                                      0.15,
                                                                      0.65,
                                                                      0.03])
        self.sensitivity_range_slider = Slider(self.sensitivity_range_slider_ax,
                                               'Sensitivity range',
                                               1,
                                               10)
        self.sensitivity_range_slider.on_changed(self._sliders_on_changed)

        # frame size (number of sample to analyse)
        self.frame_size_ax = self.fig_sliders.add_axes([0.25, 0.1, 0.65, 0.03])
        self.frame_size = Slider(self.frame_size_ax,
                                 'Frame size (smp)',
                                 1,
                                 5000.0,
                                 valinit=self.current_analysis_len)
        self.frame_size.on_changed(self._sliders_on_changed)

        # arrow key increment
        self.arrow_key_inc_ax = self.fig_sliders.add_axes([0.25, 0.05,
                                                           0.65, 0.03])
        self.arrow_key_inc = Slider(self.arrow_key_inc_ax,
                                    'arrow key inc. (smp)',
                                    1,
                                    5000.0,
                                    valinit=self.current_arrow_key_increment)
        self.arrow_key_inc.on_changed(self._sliders_on_changed)

        # create animation button
        self.animation_button = Button(self.ax_sliders, "Animate")
        self.animation_button.on_clicked(self._on_button_click)

    def _on_button_click(self, event: MouseEvent) -> None:
        """
        Callback for animation button.
        """
        # get number of frame
        start_frame = int(self.ax_IR.get_xlim()[0] * self.fs)
        end_frame = int(self.ax_IR.get_xlim()[1] * self.fs)
        total_frame = int((end_frame - start_frame) /
                          self.current_arrow_key_increment)

        print(f"Computing: {total_frame} frames")

        # compute frame
        self.current_sample = start_frame
        for i in range(total_frame):
            filename = f"Animation_{i:05d}.png"
            self._generate_energy_image(filename=filename)
            self.current_sample += self.current_arrow_key_increment

        # make animation with ffmpeg
        output_filename = f"{os.path.basename(self.filename)}.mp4"
        ff_process = subprocess.Popen(["ffmpeg",
                                       "-r", "15",
                                       "-i", "Animation_%05d.png",
                                       "-c:v", "h264",
                                       "-preset", "slow",
                                       "-b:v", "25M",
                                       output_filename])
        ff_process.wait()

        # clean temp images
        file_list = glob.glob("Animation_*.png")
        for file_path in file_list:
            os.remove(file_path)

    def _sliders_on_changed(self, val: float) -> None:
        """
        Callback for slider window.
        """
        if val == self.frame_size.val:
            print("changing analisys size: ", int(val), val / self.fs)
            self.current_analysis_len = int(val)
            self.energy = energy.Energy(self.filename, int(val))

        self.current_arrow_key_increment = int(self.arrow_key_inc.val)

        self._generate_energy_image()

    def _on_click(self, event: MouseEvent) -> None:
        """
        Callback for spectrogram window.
        Clicking on the wave will set the point where to start energy analysis.
        """
        zooming_panning = False

        if not zooming_panning and event.xdata is not None:
            self.current_sample = int(event.xdata)
            self._generate_energy_image()

    def _on_key_press(self, event: MouseEvent) -> None:
        """
        Callback for energy window.
        Pressing arrow key will change the position of the energy analysis.
        """
        if event.key == "p":
            self._generate_energy_image()
        elif event.key == "left":
            self.current_sample -= self.current_arrow_key_increment
            self._generate_energy_image()
        elif event.key == "right":
            self.current_sample += self.current_arrow_key_increment
            self._generate_energy_image()

    def _generate_energy_image(self, filename: str = None) -> None:
        """
        Main computation method to display energy from the IR.:
        :param filename: save rendered image if filenamne is provided
        """
        self.current_energy = self.energy.generate_spherical_energy_matrix(self.current_sample)

        norm = colors.LogNorm(vmin=10**-self.sensitivity_range_slider.val,
                              vmax=self.sensitivity_slider.val)

        self.ax_energy.clear()
        self.ax_energy.pcolormesh(self.energy.theta,
                                  self.energy.phi,
                                  self.current_energy,
                                  cmap=self.color_scheme,
                                  norm=norm,
                                  alpha=self.alpha
                                  )
        if self.overlayImage is not None:
            overlay = plt.imread(self.overlayImage)
            self.ax_energy.imshow(overlay, aspect='auto', extent=(0,6,-1.5,1.5))

        # add current IR time to the energy plot
        self.ax_energy.text(0, -1.5,
                            str(self.current_sample / self.fs)[0:5] + "s",
                            fontsize=28)
        self.fig_energy.canvas.draw()

        if filename is not None:
            self.fig_energy.savefig(filename, dpi=300)
        else:
            # current frame info
            print("t=", self.current_sample / self.fs,
                  " | sample:", self.current_sample,
                  " | max energy: ", self.current_energy.max(),
                  " | min energy: ", self.current_energy.min()
                  )


if __name__ == "__main__":
    alpha = 1
    color_scheme = cm.plasma

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputRIR", required=True,
                    help="Path to the ambisonic RIR")
    ap.add_argument("-p", "--inputPicture", required=False,
                    help="Path to a 360 equirectangular picture to be incorporated in the energy window")
    ap.add_argument("-a", "--alpha", required=False,
                    help="value of alpha blending if an inputPicture is provided, default to 0.7 if not provided")

    args = vars(ap.parse_args())

    if not os.path.exists(args["inputRIR"]):
        print("file missing: ", args["inputRIR"])
        exit(0)
    if args["inputPicture"] is not None:
        if not os.path.exists(args["inputPicture"]):
            print("file missing: ", args["inputPicture"])
            exit(0)
        if args["alpha"] is not None:
            alpha = float(args["alpha"])
        else:
            alpha = 0.7

    v = Visualize(args["inputRIR"],
                  overlayImage=args["inputPicture"],
                  alpha=alpha,
                  color_scheme=color_scheme)
    v.display_wav()
