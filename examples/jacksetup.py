from jacktools.jacksignal import JackSignal

import subprocess
import time
import numpy as np
import soundfile as sf

class JackSetup:
    """ Utility class to setup Jack for recording ambisonic IR.
    Currently supporting audiodice speakers, and Zylia microphones.
    To use:
    - create a JackSetup instance, specifying number of desired mic and
      buffer containing material to play.
    - connect audiodice in jack as you need them (with connect_audiodice,
      or connect_audiodice_single_speaker).
    - launch everything and save recordings with play_record.
    """

    ZYLIA_DEVICE_NAME_LIST = ["hw:CARD=ZM13E,DEV=0",
                              "hw:CARD=ZM13E_1,DEV=0",
                              "hw:CARD=ZM13E_2,DEV=0"]

    def __init__(self,
                 nb_zylia: int,
                 play_buffer: np.ndarray,
                 rec_len: int = 38) -> None:
        """
        :param nb_zylia: number of mic to use (currently up to 3).
        :param play_buffer: buffer to be played in audiodice.
        :param rec_len: length of recording in seconds (default 38s).
        """
        self.nb_zylia = nb_zylia
        self.rec_len = rec_len
        self.play_buffer = play_buffer

        # setup jacktools
        self.jacksignal = JackSignal("JackSignalESS")
        if self.jacksignal.get_state() < 0:
            print("Failed to create JackSignal -- is the server running ?")
            exit(1)
        self.name, self.fsamp, self.period = self.jacksignal.get_jack_info()

        self.jacksignal.create_output(0, "ess")

        # setup zylia mic and recording buffers
        self.alsa_process = []
        self.rec_buffer = []
        recording_len = self.rec_len * self.fsamp
        for i in range(self.nb_zylia):
            self.alsa_process.append(self._connect_zylia(i, JackSetup.ZYLIA_DEVICE_NAME_LIST[i]))
            self.rec_buffer.append(np.empty([recording_len,19],
                                            dtype = np.float32))

    def __del__(self):
        """
        Disconnect Zylia from Jack.
        """
        for i in range(self.nb_zylia):
            self.alsa_process[i].kill()

    def connect_audiodice(self,
                          audiodice_num: int) -> None:
        """
        Target a specific audiodice.
        :param audiodice_num: audiodice number.
        """
        self.jacksignal.silence()

        firstspk = 1 + 12 * (audiodice_num - 1)

        for spknum in range(firstspk, firstspk + 12):
            speakerstr = f"system:playback_{spknum}"
            self.jacksignal.connect_output(0, speakerstr)
            print(speakerstr)
            self.jacksignal.set_output_gain(spknum, 0.1)

    def connect_audiodice_single_speaker(self,
                                         audiodice_num: int,
                                         speaker_num) -> None:
        """
        Target a single speaker on a specific audiodice.
        :param audiodice_num: audiodice number.
        :param speaker_num: speaker of the audiodice.
        """
        self.jacksignal.silence()

        spknum = speaker_num + 12 * (audiodice_num - 1)
        speakerstr = f"system:playback_{spknum}"
        self.jacksignal.silence()
        self.jacksignal.connect_output(0, speakerstr)
        print(speakerstr)

        self.jacksignal.set_output_gain(spknum, 0.1)

    def disconnect_all_audiodice(self) -> None:
        """
        Disconnect all output. To use before changing speaker setup.
        """
        self.jacksignal.disconn_output(-1)

    def change_audiodice_play_buffer(self,
                                     play_buffer: np.ndarray) -> None:
        """
        :param play_buffer: new buffer to use for next play_record
                            session.
        """
        self.play_buffer = play_buffer

    def play_record(self, filename: str) -> None:
        """
        Play audio set with set_audiodice_play_buffer, and save
        recording result to file.
        :param filename: name use to save recordings. Mic number is
                         append to filename.
        """
        # set play buffer
        self.jacksignal.set_output_data(0, self.play_buffer)

        # set rec buffer
        for i in range(self.nb_zylia):
            self._set_zylia_rec_buffer(i, self.rec_buffer[i])

        self.jacksignal.process()
        self.jacksignal.wait()

        #save recorded files
        for i in range(self.nb_zylia):
            sf.write(filename+"_"+str(i)+".wav", self.rec_buffer[i],
                     self.fsamp, subtype='PCM_24')

    def stop_recording(self):
        self.jacksignal.silence()

    def reconnect_zylia(self):
        for i in range(self.nb_zylia):
            zylia_hw_id = JackSetup.ZYLIA_DEVICE_NAME_LIST[i]
            current_zylia_name = "zylia_" + str(i)
            self.alsa_process = self._expose_zylia_in_jack(zylia_hw_id,
                                                           current_zylia_name)

            for j in range(19):
                self.jacksignal.connect_input(j+i*19,
                                            current_zylia_name+":capture_"+str(j+1))

    def _expose_zylia_in_jack(self, zylia_hw_id: str,
                              zylia_name:str) -> subprocess.Popen:
        """
        Expose Zylia microphone in Jack using alsa_in utility.
        :param zylia_hw_id: hw id of zylia mic.
        :param zylia_name: name displayed in Jack.
        :return: subprocess running alsa_in.
        """
        alsa_process = subprocess.Popen(["alsa_in",
                                        "-j",
                                        zylia_name,
                                        "-d",
                                        zylia_hw_id,
                                        "-c",
                                        "19"],
                                        stdout=subprocess.PIPE)
        print("Added Zylia in Jack")
        time.sleep(2)

        return alsa_process

    def _connect_zylia(self,
                       zylia_number: int,
                       zylia_hw_id: str) -> subprocess.Popen:
        """
        Connect inside jack zylia mic to this script.
        :param zylia_hw_id: hw id of zylia mic.
        :return: subprocess running alsa_in.
        """
        current_zylia_name = "zylia_" + str(zylia_number)
        alsa_process = self._expose_zylia_in_jack(zylia_hw_id,
                                                  current_zylia_name)

        for i in range(19):
            self.jacksignal.create_input(i+zylia_number*19,
                                         current_zylia_name+"_"+str(i))
            self.jacksignal.connect_input(i+zylia_number*19,
                                          current_zylia_name+":capture_"+str(i+1))

        return alsa_process

    def _set_zylia_rec_buffer(self,
                              zylia_number: int,
                              rec_buffer: np.ndarray) -> None:
        """
        Associate recording buffer with zylia mic
        :param zylia_number: which mic to setup
        :param rec_buffer: recording buffer to use with this mic
        """
        if zylia_number >= self.nb_zylia:
            raise Exception(f"Mic {zylia_number} not present.")

        for i in range(19):
            self.jacksignal.set_input_data(i+zylia_number*19, rec_buffer[:,i])

