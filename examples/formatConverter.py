from tokenize import String
import sys
import os

import ambir.utils.toolBox as tb

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('Provide path')
        print('python3 ./formatConverter.py <PATH TO AMBIXs>')
        exit(0)

    wav_path = sys.argv[1]
    measurements = tb.get_ambisonic_recording(wav_path)
    print(measurements)
    for measure in measurements:
        try:
            new_file = tb.convert_SN3D_to_N3D(measure)
            print(new_file)
        except Exception as e:
            print(e)

